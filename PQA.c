#include<stdio.h>
#include<stdlib.h>
#include <time.h>

/*
Escola Polit�cnica de Pernambuco

Disciplina Teoria da Computa��o

Aluno: Alefe Cruz da Silva/
		
Problema Quadr�tico de Aloca��o PQA
*/


int** criaMatriz(int tamMatriz){ //Recebe a quantidade de Linhas e Colunas como Par�metro
 
  int i,j; //Vari�veis Auxiliares
 
  int **m = (int**)malloc(tamMatriz * sizeof(int*)); //Aloca um Vetor de Ponteiros
 
  for (i = 0; i < tamMatriz; i++){ //Percorre as linhas do Vetor de Ponteiros
       m[i] = (int*) malloc(tamMatriz * sizeof(int)); //Aloca um Vetor de Inteiros para cada posi��o do Vetor de Ponteiros.
       for (j = 0; j < tamMatriz; j++){ //Percorre o Vetor de Inteiros atual.
            if(i==j)
            	m[i][j] = 0;
            else if(i<j){	
				m[i][j] = rand()%100+1; //Escreve um numero aleat�rio entre 0 e 99.
			}else
				m[i][j] = m[j][i]; // Espelha a matriz.
       }
  }
return m; //Retorna o Ponteiro para a Matriz Alocada
}

void imprimeMatriz(int tamMatriz, int **matriz){// Mostra um matriz seja fluxo ou distancia na tela
	int i, j;
	
	for (i = 0; i < tamMatriz; i++){ 
       for (j = 0; j < tamMatriz; j++){ 
            printf("%d ",matriz[i][j] );
            if(matriz[i][j] < 10)
            	printf(" ");
       }
       		printf("\n");
 	}
 	
 	return;
}

void liberaMatriz(int tamMatriz, int **matriz){// Libera memoria alocada pela matriz Generica
	
	int i;
	for (i=0; i<tamMatriz; i++) 
	free (matriz[i]); //Libera as linhas da matriz
  	
	free (matriz);//Libera a matriz 
}
void liberaVetor(int *vetor){//Libera memoria alocado pelo vetor Gen�rico
	free (vetor);
	return;
}

int tamanhoVetor(int tamMatriz){//Calcula o tamanhodo vetor apartir do tamanho da matriz
	int tamVetor= 0,i;
	
	for(i=0; i< tamMatriz ; i++ )
		tamVetor = tamVetor + i;
	
	return tamVetor;
}

int *criaVetor(int tamMatriz, int **matriz){//Cria  um vetor apartir dos valores da matriz, excluindo a diagonal principal e os valores abaixo dela
	int i,j,k=0,*vetor;
	int tamVetor = tamanhoVetor(tamMatriz);
	
	vetor = (int *) malloc(tamVetor * sizeof(int));	
	
	for (i = 0; i < tamMatriz; i++){ 
       for (j = 0; j < tamMatriz; j++){ 
            if(i>j){
            	vetor[k] = matriz[i][j];
            	k++;
	   		}
	   }
 	}
 				
	return vetor;
}

void imprimeVetor(int tamMatriz,int *vetor){ //Mostra um vetor Gen�rico na tela
	int tamVetor,i;
	tamVetor = tamanhoVetor(tamMatriz);
	printf("\nVetor Distancia Permultado: ");	
	for (i = 0; i < tamVetor; i++){ 
            	printf("%d ",vetor[i]);
    }
	printf("\n\n");
	return;
}

int **permuta(int **matriz,int a, int b,int tamMatriz){ // Permuta a matriz distancia entre 2 linhas e 2 coluna diferentes ao mesmo tempo
	int i,j;
	
	int **m = (int**)malloc(tamMatriz * sizeof(int*));
	
	for (i = 0; i < tamMatriz; i++){ 
		m[i] = (int*) malloc(tamMatriz * sizeof(int));
      	for (j = 0; j < tamMatriz; j++){ 
			if(i==a && j==b || i==b && j ==a|| i== a && j == a|| i==b && j==b || i!=a && i!=b && j!=a && j!= b ){
						m[i][j] = matriz[i][j];//valores que nao permutam
			}else if(i==a && j!=b){
						m[a][j] = matriz[b][j];//valores que permutam
			
			}else if(i==b && j!=a){
						m[b][j] = matriz[a][j];//valores que permutam
			
			}else if(j==a && i!=b){
						m[i][a] = matriz[i][b];//valores que permutam
			
			}else if(j==b && i!=a){
						m[i][b] = matriz[i][a];	//valores que permutam
			}
				 
		
       }
 }
	
	return m;	
}

int Valor(int *vetorFluxo,int *vetorDistancia, int tamMatriz){//Calcula o custo totalde alocacao apartir dos 2 vetores
	int i,custoTotal=0,tamVetor;
	tamVetor = tamanhoVetor(tamMatriz);
		for (i = 0; i < tamVetor; i++){ 
			if(i!=0 && i!=tamVetor)
				printf("+ ");
				
			printf("(%d * %d) ",vetorFluxo[i],vetorDistancia[i]);
      		custoTotal = vetorDistancia[i]*vetorFluxo[i] + custoTotal;//somatorio das multiplicacoes
   		 }
    printf("\n");
	return custoTotal;
}



int main(){	
	//Variaveis	
	int **matrizFluxo; 
	int **matrizDistancia; 
	int *vetorFluxo;
	int *vetorDistancia;
	int tamMatriz = 0;
	int i,j,total,menorCusto=0,controle = 1,k,cont,a,b;
	
	//Interacao com usuario para tamanho das matrizes
		
	do{
		printf("Matriz Fluxo/Matriz Distancia \n");
		printf("Digite o tamanhos das Matrizes: ");
		scanf("%d",&tamMatriz);
		system("cls");	
	}while(tamMatriz <= 0);
	
	//cria as matrizes
	matrizFluxo = criaMatriz(tamMatriz);
	matrizDistancia = criaMatriz(tamMatriz);
	
	//mostra as matrizes com valores random
	printf("*** Matriz de Fluxo \n\n");
	imprimeMatriz(tamMatriz,matrizFluxo);
	printf("\n\n*** Matriz de Distancia \n\n");
	imprimeMatriz(tamMatriz,matrizDistancia);
	
	//cria os vetores apartirs das matrizes
	vetorFluxo =  criaVetor(tamMatriz,matrizFluxo);
	vetorDistancia =  criaVetor(tamMatriz,matrizDistancia);
	
	printf("\n\n*** Vetor de Fluxo \n\n");
	imprimeVetor(tamMatriz, vetorFluxo);
	printf("\n\n*** Vetor de Distancia \n\n");
	imprimeVetor(tamMatriz, vetorDistancia);
	
	//custo de alocacao sem permutar
	total = Valor(vetorFluxo,vetorDistancia, tamMatriz)	;
	printf("*custo de Alocacao total : %d \n",total);
	menorCusto= total;
	cont= 0;
	
	//teste
	for(i=tamMatriz - 2; i > 1 ; i--)
		controle = i*controle;

	
	
	
	for(k=0; k<controle;k++){//imprime todas as permuta��es
	
		for(i=0;i<tamMatriz;i++){
			for(j=0;j<tamMatriz;j++){
				if(i!=j){
					printf("\n\n *** Permutacao %d %d \n\n",i,j);
				
						if(i+k < tamMatriz && j+k < tamMatriz)
							matrizDistancia = permuta(matrizDistancia, i+k , j+k ,tamMatriz);
						else
						 	matrizDistancia = permuta(matrizDistancia, (i+k) % tamMatriz, 	(j+k) % tamMatriz ,tamMatriz);
					 	
					imprimeMatriz(tamMatriz,matrizDistancia);
					vetorDistancia =  criaVetor(tamMatriz,matrizDistancia);
					imprimeVetor(tamMatriz, vetorDistancia);
					total = Valor(vetorFluxo,vetorDistancia, tamMatriz)	;
					printf("\n*custo de Alocacao total : %d \n",total);
					cont=cont+1; //Conta o numero de permuta��es
					if(menorCusto > total)
						menorCusto = total;//Menor Custo de todas as itera�oes
				
				}
			}
		}
	}
	
	printf("\n\n\n-> Menor Custo de todas iteracoes : %d\n",menorCusto);
	printf("-> Numero de Itera�oes : %d\n \n\n",cont);
	//Libera memoria
	liberaMatriz(tamMatriz, matrizFluxo);
	liberaMatriz(tamMatriz, matrizDistancia);
	liberaVetor(vetorFluxo);
	liberaVetor(vetorDistancia);
	
	system("pause");
	return 0;
}
